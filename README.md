# 
## 

- GSAP with React Hooks 
First thing to tackle was how to implement this plugin in React, and then the best way to implement with React Hooks.

Add gsap to your project like any other React app, with yarn or npm. Don't forget to register your plugin. After that, the name of the game is useRefs so that we can access the elements imperatively. Here, I add each Year list item and each card to the yearRefs current array.

```js
// Timeline.js
import React, { useEffect, useRef } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);
gsap.core.globals('ScrollTrigger', ScrollTrigger);

const timeline = ({ timeline }) => {
  const yearRefs = useRef([]);
  yearRefs.current = [];

  const addToYearRefs = el => {
    if (el && !yearRefs.current.includes(el)) {
      yearRefs.current.push(el);
    }
  };


return (
   ....
   <YearListItem
        value={eventsPerYear.year}
        key={i}
        ref={addToYearRefs}
    >
        ...
        <Card ref={addToYearRefs} />
        ...
    </YearListItem>
)
};
```
```js
// Card.js

import React from 'react';

const card = React.forwardRef((ref) => {
  return (
    <Card ref={ref} >
    </Card>
  );
});
``` 

- ScrollTrigger for Year, Event Cards 
I was able to set up the ```scrollTrigger```` for each year on the timeline quite easily after consuming the official documentation and a handful of GSAP forum posts.