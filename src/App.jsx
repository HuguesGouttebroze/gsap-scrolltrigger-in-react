import { useState, useRef, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import { gsap, ScrollTrigger } from "gsap/all";

// don't forget to register plugins
gsap.registerPlugin(ScrollTrigger); 

function Box({ children, timeline, index }) {
  const el = useRef();
  // add "100 px to left" animation on timeline
  useEffect(() => {
    timeline.to(el.current, { x: -100 }, index * 0.1);
  }, [timeline]);

  return <div id="Box" className='box' ref={el}>{children}</div>
}

function Circle({ children, timeline, index, rotation }) {
  const el = useRef();
  // add "right 100px, rotate 360deg" animation on timeline
  useEffect(() => {
    timeline.to(el.current, { rotate: rotation, x: 100 }, index * 0.1);
  }, [timeline, rotation]);

  return <div id="Circle" className='circle' ref={el}>{children}</div>
}

function App() {
  const [t1, setT1] = useState(() => gsap.timeline());
  const circleRef = useRef(null);
  const [count, setCount] = useState(0);
  
  useEffect(() => {
    gsap.to("#thirdCircle", {
      x: 100,
      duration: 2,
      ease: "bounce",
      delay: 1,
      scrollTrigger: {
        trigger: "#thirdCircle",
        markers: true,
        start: "top center",
        end: "bottom 80px",
        scrub: true
      },

    });
  }, []);

/*   gsap.set(".follower",{xPercent:-50,yPercent:-50});
  gsap.set(".cursor",{xPercent:-50,yPercent:-50});
      
      var f = document.querySelector(".follower");
      var c = document.querySelector(".cursor");

  window.addEventListener("mousemove",e => {
    gsap.to(c,0.2,{x:e.clientX, y:e.clientY})
    gsap.to(f,0.9,{x:e.clientX, y:e.clientY})
  }) */

  return (
    <div className="App">
      <Box timeline={t1} index={0}>BOX</Box>
      <Circle timeline={t1} rotation={360} index={1}>CIRCLE</Circle>
      <div id='firstCircle'>
        1er CERCLE
      </div>
      <div id='secondCircle'>2eme CERCLE</div>
      <div ref={circleRef} id='thirdCircle'>3eme CERCLE</div>
    </div>
  )
}

export default App
