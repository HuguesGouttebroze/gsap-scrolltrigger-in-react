import React, { useEffect, useRef } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);
gsap.core.globals('ScrollTrigger', ScrollTrigger);

const timeline = ({ timeline }) => {
  const yearRefs = useRef([]);

  useEffect(() => {
    gsap.from('#line', {
      scrollTrigger: {
        trigger: 'body',
        scrub: true,
        start: 'top bottom',
        endTrigger: '#footer',
        end: 'top top',
        snap: {
          snapTo: yearRefs,
          duration: { min: 0.2, max: 3 },
          delay: 0.2,
          ease: 'power1.inOut',
        },
      },
      scaleY: 0,
      transformOrigin: 'top, top',
      ease: 'none',
    });
  })
  yearRefs.current = [];

  const addToYearRefs = el => {
    if (el && !yearRefs.current.includes(el)) {
      yearRefs.current.push(el);
    }
  };


return (
   <YearListItem
        value={eventsPerYear.year}
        key={i}
        ref={addToYearRefs}
    >
        <Card ref={addToYearRefs} />
    </YearListItem>
)
};
